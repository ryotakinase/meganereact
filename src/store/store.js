import { createStore as reduxCreateStore, combineReducers } from 'redux'
import { resultReducer, typeReducer, loginUserReducer, loginReducer } from '../reducer/reducers';
import { QuestionListReducer } from '../reducer/reducer';

export default function createStore(){
  return reduxCreateStore(
    combineReducers({
      results: resultReducer,
      types: typeReducer,
      loginUser: loginUserReducer,
      isLogined: loginReducer,
	    questionList: QuestionListReducer    
    })
  )
}