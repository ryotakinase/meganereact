import React from 'react'
import { withRouter } from "react-router";
import Result from '../result/result';
import { Row, Col} from 'react-bootstrap'
import { withCookies, useCookies } from 'react-cookie';

function Top() {
  const [cookies] = useCookies(['user']);
  console.log(cookies)
  return (
    <>
      <Row>
        <Col md="1" />
        <Col md="10">
          <div>
            <p className="text-right mt-4">お疲れ様です! {cookies.user.name}さん</p>
            <Result />
          </div>
        </Col>
        <Col md="1" />
      </Row>
    </>
  )
}

export default  withCookies(withRouter(Top));
