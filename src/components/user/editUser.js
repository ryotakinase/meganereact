import React, { useState } from "react"
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import 'whatwg-fetch'
import { Row, Col } from 'react-bootstrap'
import '../../css/user.css'
import { withCookies, useCookies } from 'react-cookie';

function EditUser(props) {

  // ログインユーザーの取得
  const [cookies] = useCookies(['user']);
  const loginUser = cookies.user

  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")

  const [passwordError, setPasswordError] = useState(false)
  const [confirmPasswordError, setConfirmPasswordError] = useState(false)
  const [confirmPasswordDifError, setConfirmPasswordDifError] = useState(false)
  const [passwordFormatError, setPasswordFormatError] = useState(false)

  const inputPassword = (e) => {
    setPassword(e.target.value)
  }

  const inputConfirmPassword = (e) => {
    setConfirmPassword(e.target.value)
  }

  const validationCheckBeforeSubmit = () => {
    let count = 0;

    if (password.length === 0 || password.length > 20) {
      setPasswordError(true)
      count++
    } else {
      setPasswordError(false)
    }

    if (password.match("[^A-Za-z0-9]")) {
      setPasswordFormatError(true)
      count++
    } else {
      setPasswordFormatError(false)
    }

    if (confirmPassword.length === 0 || confirmPassword.length > 20) {
      setConfirmPasswordError(true)
      count++
    } else {
      setConfirmPasswordError(false)
    }

    if (confirmPassword !== password) {
      setConfirmPasswordDifError(true)
      count++
    } else {
      setConfirmPasswordDifError(false)
    }

    if (count === 0) {
      postSend()
    } else {
      return
    }
  }

  const postSend = () => {
    const data = {
      id: loginUser.id,
      password: password
    }
    fetch("http://localhost:8080/user/edit", {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then((response) => {
        if (response.status === 200) {
          props.history.push("/")
        } else {
          alert("更新に失敗しました。")
        }
      })
      .catch(error => console.error(error))
  }

  return (
    <>
      <Row>
        <Col md="3" />
        <Col md="6">
          <h2 className="signup-title">パスワード変更</h2>
          <table className="table table-bordered signup-table">
            <tbody>
              <tr>
                <th>パスワード</th>
                <td>
                  {passwordError && <p className="errorMessage">パスワードは1~20文字で入力してください</p>}
                  {passwordFormatError && <p className="errorMessage">半角英数字(大小文字区別含む)のみ入力可能です</p>}
                  <input type="password" size="30" value={password} onChange={inputPassword} />
                </td>
              </tr>
              <tr>
                <th>確認用パスワード</th>
                <td>
                  {confirmPasswordError && <p className="errorMessage">確認用パスワードを入力してください</p>}
                  {confirmPasswordDifError && <p className="errorMessage">確認用パスワードが上記パスワードと異なります</p>}
                  <input type="password" size="30" value={confirmPassword} onChange={inputConfirmPassword} />
                </td>
              </tr>
            </tbody>
          </table>
          <div className="text-center">
            <label className="signup-button" onClick={validationCheckBeforeSubmit}>　更新　</label>
          </div>
        </Col>
        <Col md="3" />
      </Row>
    </>
  )

}

EditUser.propTypes = {
  history: PropTypes.object
}

export default withCookies(withRouter(EditUser))