import React, { useState, useEffect, useRef } from "react"
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import 'whatwg-fetch'
import { Row, Col } from 'react-bootstrap'
import '../../css/user.css'
import { withCookies, useCookies } from 'react-cookie';

function Signup(props) {
  const [adminFlg, setAdminFlg] = useState(false)
  //const [createdDate, setCreatedDate] = useState(new Date())

  const [accountError, setAccountError] = useState(false)
  const [nameError, setNameError] = useState(false)
  const [teamError, setTeamError] = useState(false)
  const [passwordError, setPasswordError] = useState(false)
  const [confirmPasswordError, setConfirmPasswordError] = useState(false)
  const [confirmPasswordDifError, setConfirmPasswordDifError] = useState(false)
  const [accountFormatError, setAccountFormatError] = useState(false)
  const [nameFormatError, setNameFormatError] = useState(false)
  const [passwordFormatError, setPasswordFormatError] = useState(false)
  const [accountDuplicateError, setAccountDuplicateError] = useState(false)
  const [isAction, setIsAction] = useState(false)

  const [users, setUsers] = useState([])
  const [teams, setTeams] = useState([])

  const [cookies] = useCookies(['user']);

  //依存性のある変数をuseEffectで使用する際のレンダリング時に置ける同一性判断
  const prevCookies = useRef(cookies);

  //(componentDidMountと同作用)
  useEffect(() => {
    //管理権限チェック
    if(!cookies.user.adminFlg){
      alert("管理権限を持つアカウントでアクセスしてください\nトップに戻ります")
      window.location.href = "../"
    }

    //アカウント重複チェックのためのユーザー取得
    fetch("http://localhost:8080/user/get", {
      method: "GET"
    }).then((response) => {
      if (response.status === 200) {
        response.json().then(json => {
          setUsers(json)
        })
      } else {
        alert("通信エラー:users取得")
      }
    }).catch(error => console.log(error));
    //所属チーム取得
    fetch("http://localhost:8080/team/get", {
      method: "GET"
    }).then((response) => {
      if (response.status === 200) {
        response.json().then(json => {
          setTeams(json)
        })
      } else {
        alert("通信エラー:teams取得")
      }
    }).catch(error => console.log(error));

    prevCookies.current = cookies;
  }, [cookies, isAction])

  const inputAdminFlg = (e) => {
    if (e.target.value === "admin") {
      setAdminFlg(true)
    } else {
      setAdminFlg(false)
    }
  }

  const validationCheckBeforeSubmit = () => {
    //値の取得
    let account = document.getElementById("account").value;
    let name = document.getElementById("name").value;
    let team = document.getElementById("team").value;
    let password = document.getElementById("password").value
    let confirmPassword = document.getElementById("confirmPassword").value

    //バリデーション
    let count = 0;

    if (account.length === 0 || account.length > 20) {
      setAccountError(true)
      count++
    } else {
      setAccountError(false)
    }

    if (account.match("[^0-9]+")) {
      setAccountFormatError(true)
      count++
    } else {
      setAccountFormatError(false)
    }

    if (name.length === 0 || name.length > 20) {
      setNameError(true)
      count++
    } else {
      setNameError(false)
    }

    if (name.match("[^ぁ-んァ-ヶ一-龠a-zA-Z]")) {
      setNameFormatError(true)
      count++
    } else {
      setNameFormatError(false)
    }


    if (password.length === 0 || password.length > 20) {
      setPasswordError(true)
      count++
    } else {
      setPasswordError(false)
    }

    if (password.match("[^A-Za-z0-9]")) {
      setPasswordFormatError(true)
      count++
    } else {
      setPasswordFormatError(false)
    }

    if (team === "") {
      setTeamError(true)
      count++
    } else {
      setTeamError(false)
    }

    if (confirmPassword.length === 0 || confirmPassword.length > 20) {
      setConfirmPasswordError(true)
      count++
    } else {
      setConfirmPasswordError(false)
    }

    if (confirmPassword !== password) {
      setConfirmPasswordDifError(true)
      count++
    } else {
      setConfirmPasswordDifError(false)
    }

    let accountMatch = false
    users.forEach(user => {
      if (account === user.account) {
        accountMatch = true;
        return;
      }
    });
    if (accountMatch) {
      setAccountDuplicateError(true)
      count++
    } else {
      setAccountDuplicateError(false)
    }

    if (count === 0) {
      postSend(account, name, team, password)
    } else {
      return
    }
  }

  const postSend = (docAccount, docName, docTeam, docPassword) => {
    const data = {
      account: docAccount,
      name: docName,
      team: docTeam,
      password: docPassword,
      adminFlg: adminFlg
    }
    fetch("http://localhost:8080/user/add", {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then((response) => {
        if (response.status === 200) {
          alert("登録しました")
          props.history.push("/")
        } else {
          alert("登録に失敗しました")
        }
      })
      .catch(error => console.error(error))
  }

  const addTeam = () => {
    let newTeam = window.prompt('追加する部署を入力してください')
    const data = {
      name: newTeam
    }
    fetch("http://localhost:8080/team/add", {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then((response) => {
        if (response.status === 200) {
          alert("登録しました")
          if(isAction){
            setIsAction(false)
          } else {
            setIsAction(true)
          }
          props.history.push("/signup/")
        } else {
          alert("登録に失敗しました")
        }
      })
      .catch(error => console.error(error))
  }

  return (
    <>
      <Row>
        <Col md="3" />
        <Col md="6">
          <h2 className="signup-title">社員登録</h2>
          <table className="table table-bordered signup-table">
            <tbody>
              <tr>
                <th>社員番号</th>
                <td>
                  {accountError && <p className="errorMessage">社員番号は1~20文字で入力してください</p>}
                  {accountFormatError && <p className="errorMessage">半角数字のみ入力可能です</p>}
                  {accountDuplicateError && <p className="errorMessage">既に存在するアカウントIDです</p>}
                  <input type="text" size="30" id="account" />
                </td>
              </tr>
              <tr>
                <th>名前</th>
                <td>
                  {nameError && <p className="errorMessage">名前は1~20文字で入力してください</p>}
                  {nameFormatError && <p className="errorMessage">記号(スペース含む)や数字を用いず入力してください</p>}
                  <input type="text" size="30" id="name" />
                </td>
              </tr>
              <tr>
                <th>所属</th>
                <td>
                  {teamError && <p className="errorMessage">所属を選択してください</p>}
                  <select id="team" style={{ width: '320px' }}>
                    <option value="">選択してください</option>
                    {teams.map(team => <option value={team.id}>{team.name}</option>)}
                  </select>
                  <button onClick={addTeam} className="btn-sm btn-secondry mt-2">
                    所属を追加する
                  </button>
                </td>
              </tr>
              <tr>
                <th>パスワード</th>
                <td>
                  {passwordError && <p className="errorMessage">パスワードは1~20文字で入力してください</p>}
                  {passwordFormatError && <p className="errorMessage">半角英数字(大小文字区別含む)のみ入力可能です</p>}
                  <input type="password" id="password" size="30" />
                </td>
              </tr>
              <tr>
                <th>確認用パスワード</th>
                <td>
                  {confirmPasswordError && <p className="errorMessage">確認用パスワードを入力してください</p>}
                  {confirmPasswordDifError && <p className="errorMessage">確認用パスワードが上記パスワードと異なります</p>}
                  <input type="password" id="confirmPassword" size="30" />
                </td>
              </tr>
              <tr>
                <th>権限</th>
                <td>
                  <strong className="admin-select">
                    <input type="radio" value="admin" onChange={inputAdminFlg} checked={adminFlg === true} />管理者
                  </strong>
                  <strong className="admin-select">
                    <input type="radio" value="nomal" onChange={inputAdminFlg} checked={adminFlg === false} />一般
                  </strong>
                </td>
              </tr>
            </tbody>
          </table>
          <div className="text-center">
            <label className="signup-button" onClick={validationCheckBeforeSubmit}>　登録　</label>
          </div>
        </Col>
        <Col md="3" />
      </Row>
    </>
  )

}

Signup.propTypes = {
  history: PropTypes.object
}

export default withCookies(withRouter(Signup))