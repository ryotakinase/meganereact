import React, { useEffect, useState, useRef } from 'react'
import { useParams } from 'react-router-dom';
import ResultChart from '../result/ResultChart';
import '../../css/user.css'
import { withCookies } from 'react-cookie';
import { withRouter } from "react-router";

function UserDetail(props) {
  const [userResult, setUserResult] = useState(null)
  // 下記のコードでURLからパラメータを取得できるので、user一覧から遷移する際にuserのIDを渡してください
  const userId = useParams();
  //依存性のある変数をuseEffectで使用する際のレンダリング時に置ける同一性判断
  const prevUserId = useRef(userId);

  useEffect(() => {
    fetch("http://localhost:8080/result/get/user/" + userId.id, {
      method: "GET"
    })
    .then(response => response.json())
    .then(data => setUserResult(data))
    prevUserId.current = userId;
  }, [userId])

  return (
    <div>
      {userResult === null || userResult.length === 0 ?
        <div className="text-center no-test-name">
          <h2>テスト未実施です</h2>
        </div>
      :
        <ResultChart resultList={userResult} />
      }
    </div>
  )
}

export default withCookies(withRouter(UserDetail));