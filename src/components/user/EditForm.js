import React, { useState, useEffect, useRef } from "react"
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import 'whatwg-fetch'
import { Row, Col } from 'react-bootstrap'
import '../../css/user.css'
import { withCookies, useCookies } from 'react-cookie';

function EditForm(props) {

  const user = props.editUser
  const [adminFlg, setAdminFlg] = useState(user.adminFlg)
  const [team, setTeam] = useState(user.team)


  const [accountError, setAccountError] = useState(false)
  const [nameError, setNameError] = useState(false)
  const [accountFormatError, setAccountFormatError] = useState(false)
  const [nameFormatError, setNameFormatError] = useState(false)
  const [accountDuplicateError, setAccountDuplicateError] = useState(false)

  const [users, setUsers] = useState([])
  const [teams, setTeams] = useState([])
  const [isAction, setIsAction] = useState(false)

  const [cookies] = useCookies(['user']);

  const prevCookies = useRef(cookies);

  useEffect(() => {
    //管理権限チェック
    if (!cookies.user.adminFlg) {
      alert("管理権限を持つアカウントでアクセスしてください\nトップに戻ります")
      window.location.href = "../"
    }

    //アカウント重複チェックのためのユーザー取得
    fetch("http://localhost:8080/user/get", {
      method: "GET"
    }).then((response) => {
      if (response.status === 200) {
        response.json().then(json => {
          setUsers(json)
        })
      } else {
        alert("通信エラー:users取得")
      }
    }).catch(error => console.log(error));
    //所属チーム取得
    fetch("http://localhost:8080/team/get", {
      method: "GET"
    }).then((response) => {
      if (response.status === 200) {
        response.json().then(json => {
          setTeams(json)
        })
      } else {
        alert("通信エラー:teams取得")
      }
    }).catch(error => console.log(error));
    prevCookies.current = cookies;
  }, [cookies, isAction])

  const inputAdminFlg = (e) => {
    if (e.target.value === "admin") {
      setAdminFlg(true)
    } else {
      setAdminFlg(false)
    }
  }
  const inputTeam = (e) => {
    setTeam(e.target.value)
  }


  const validationCheckBeforeSubmit = () => {
    //値の取得
    let account = document.getElementById("account").value;
    let name = document.getElementById("name").value;;

    //バリデーション
    let count = 0;

    if (account.length === 0 || account.length > 20) {
      setAccountError(true)
      count++
    } else {
      setAccountError(false)
    }

    if (account.match("[^0-9]+")) {
      setAccountFormatError(true)
      count++
    } else {
      setAccountFormatError(false)
    }

    if (name.length === 0 || name.length > 20) {
      setNameError(true)
      count++
    } else {
      setNameError(false)
    }

    if (name.match("[^ぁ-んァ-ヶ一-龠a-zA-Z]")) {
      setNameFormatError(true)
      count++
    } else {
      setNameFormatError(false)
    }

    let accountMatch = false
    users.forEach(u => {
      if (account === u.account && account !== user.account) {
        accountMatch = true;
        return;
      }
    });
    if (accountMatch) {
      setAccountDuplicateError(true)
      count++
    } else {
      setAccountDuplicateError(false)
    }

    if (count === 0) {
      postSend(account, name, team)
    } else {
      return
    }
  }

  const postSend = (docAccount, docName, docTeam) => {
    const data = {
      id: user.id,
      account: docAccount,
      name: docName,
      team: docTeam,
      adminFlg: adminFlg
    }
    fetch("http://localhost:8080/user/update", {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then((response) => {
        if (response.status === 200) {
          if (cookies.user.id === user.id) {
            fetch("http://localhost:8080/user/get" + user.id, {
              method: "GET"
            })
            .then((response) => {
              if (response.status === 200) {
                response.json()
                  .then(json => {
                    cookies.set('user', json, { path: "/" });
                  })
              }
            }).catch(error => console.error(error));
          }
          console.log(cookies.user)
          alert("更新しました")
          props.history.push("/")
        } else {
          alert("更新に失敗しました。")
        }
      })
      .catch(error => console.error(error))
  }

  const addTeam = () => {
    let newTeam = window.prompt('追加する部署を入力してください')
    const data = {
      name: newTeam
    }
    fetch("http://localhost:8080/team/add", {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then((response) => {
        if (response.status === 200) {
          alert("登録しました")
          if(isAction){
            setIsAction(false)
          } else {
            setIsAction(true)
          }
          props.history.push("/users/edit/" + user.id)
        } else {
          alert("登録に失敗しました")
        }
      })
      .catch(error => console.error(error))
  }

  return (
    <>
      <Row>
        <Col md="3" />
        <Col md="6">
          <h2 className="signup-title">社員情報変更</h2>
          <table className="table table-bordered signup-table">
            <tbody>
              <tr>
                <th>社員番号</th>
                <td>
                  {accountError && <p className="errorMessage">社員番号は1~20文字で入力してください</p>}
                  {accountFormatError && <p className="errorMessage">半角数字のみ入力可能です</p>}
                  {accountDuplicateError && <p className="errorMessage">既に存在するアカウントIDです</p>}
                  <input type="text" id="account" size="30" defaultValue={user.account} />
                </td>
              </tr>
              <tr>
                <th>名前</th>
                <td>
                  {nameError && <p className="errorMessage">名前は1~20文字で入力してください</p>}
                  {nameFormatError && <p className="errorMessage">記号(スペース含む)や数字を用いず入力してください</p>}
                  <input type="text" id="name" size="30" defaultValue={user.name} />
                </td>
              </tr>
              <tr>
                <th>所属</th>
                <td>
                  <select id="team" style={{ width: '320px' }} value={team} onChange={inputTeam}>
                    {teams.map(t => <option value={t.id}>{t.name}</option>)}
                  </select>
                  <button onClick={addTeam} className="btn-sm btn-secondry mt-2">
                    所属を追加する
                  </button>
                </td>
              </tr>
              {cookies.user.id === user.id ?
                <>
                </>
              :
              <tr>
                <th>権限</th>
                <td>
                  <strong className="admin-select">
                    <input type="radio" value="admin" onChange={inputAdminFlg} checked={adminFlg === true} />管理者
                  </strong>
                  <strong className="admin-select">
                    <input type="radio" value="nomal" onChange={inputAdminFlg} checked={adminFlg === false} />一般
                  </strong>
                </td>
              </tr>
              }
            </tbody>
          </table>
          <div className="text-center">
            <label className="signup-button" onClick={validationCheckBeforeSubmit}>　更新　</label>
          </div>
        </Col>
        <Col md="3" />
      </Row>
    </>
  )

}

EditForm.propTypes = {
  history: PropTypes.object
}

export default withCookies(withRouter(EditForm))