import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from '../../action/actions';
import PropTypes from 'prop-types';
import { withCookies } from 'react-cookie';
import { Form, Button, Alert } from 'react-bootstrap';


class AddUser extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.state = {
            account: "",
            name: "",
            password: "",
            users: [],
            isAccountError: false,
            accountErrMsg: "",
        }
    }

    componentDidMount() {
        // ユーザー一覧を取得
        fetch("http://localhost:8080/user/get", {
            method: "GET"
        }).then((response) => {
            if (response.status === 200) {
                response.json().then(json => {
                    this.setState({ users: json })
                })
            } else {
                alert("通信エラー")
            }
        }).catch(error => console.log(error));
    }

    handleAccountChange(e) {
        const value = e.target.value;
        this.setState({ account: value, });
        // アカウント重複チェック
        let accountMatch = false
        this.state.users.forEach(user => {
            if (value == user.account) {
                accountMatch = true;
                return;
            }
        });
        if (accountMatch) {
            this.setState({
                isAccountError: true,
                accountErrMsg: "アカウントが重複しています"
            })
        } else {
            this.setState({
                isAccountError: false,
                accountErrMsg: ""
            })
        }
    }
    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    // 新規登録(管理者側のみで可能)
    AddSubmit() {
        console.log(this.state.isAccountError)
        if (this.state.isAccountError) {
            alert("アカウント名を修正してください")
        } else {
            const data = { account: this.state.account, name: this.state.name, password: this.state.password, referenceDate: this.state.referenceDate };
            fetch("http://localhost:8080/user/add", {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            }).then((response) => {
                if (response.status === 200) {
                    //正常に登録できた場合はuser一覧がリターンされる(予備)
                    window.location.href = "../"
                } else {
                    alert("登録を正常に実行できませんでした。");
                }
            }).catch(error => console.log(error));
        }
    }

    render() {
        return (
            <>

                <div className="signup-area">
                    {this.state.isMissSignup &&
                        <Alert variant="danger text-center" className="login-error-alert">
                            <p>登録に失敗しました</p>
                        </Alert>
                    }

                    <Form className="input-regist-form">
                        <Form.Group className="form-object" controlId="formBasicAccount">
                            <Form.Label className="form-label">アカウント</Form.Label>
                            <div className="form-error">
                                <p className="err-msg">
                                    {this.state.isAccountError && this.state.accountErrMsg}
                                </p>
                                <Form.Control className="" onChange={(e) => this.handleAccountChange(e)} value={this.state.account} type="text" placeholder="半角英数字のみ使えます" />
                                <p className="length-check"><span className="account-length" id="accountLen">{this.state.accountLength}</span>/20</p>
                            </div>
                        </Form.Group>
                        <Form.Group className="form-object" controlId="formBasicName">
                            <Form.Label className="form-label">氏名</Form.Label>
                            <div className="form-error">

                                {this.state.isNameError && <p className="err-msg">{this.state.nameErrMsg}</p>}

                                <Form.Control className="" onChange={(event) => this.handleNameChange(event)} value={this.state.name} type="text" placeholder="記号は使えません" />
                                <p className="length-check mb-0"><span className="name-length" id="nameLen">{this.state.nameLength}</span>/10</p>
                            </div>
                        </Form.Group>
                        <Form.Group className="form-object" controlId="formBasicReferenceDate">
                            <Form.Label className="form-label">基準日</Form.Label>
                            <div className="form-error">
                                <p className="err-msg">
                                    {this.state.isReferenceError && this.state.referenceErrMsg}
                                </p>
                                <Form.Control className="" onChange={(event) => this.handleDateChange(event)} value={this.state.referenceDate} type="Date" />
                            </div>
                        </Form.Group>
                        <Form.Group className="form-object" controlId="formBasicPassword">
                            <Form.Label className="form-label">パスワード</Form.Label>
                            <div className="form-error">
                                <p className="err-msg">
                                    {this.state.isPasswordError && this.state.passwordErrMsg}
                                </p>
                                <Form.Control className="" onChange={(event) => this.handlePasswordChange(event)} value={this.state.password} type="password" placeholder="半角英数字と記号が使えます" />
                                <p className="length-check"><span className="password-length" id="passwordLen">{this.state.passwordLength}</span>/20</p>
                            </div>
                        </Form.Group>
                        <Form.Group className="form-object" controlId="formBasicPasswordConfirmation">
                            <Form.Label className="form-label">パスワード(確認用)</Form.Label>
                            <div className="form-error">
                                <Form.Control className="" onChange={(event) => this.handlePasswordConfirmationChange(event)} value={this.state.passwordConfirmation} type="password" />
                                <p className="length-check" id="passwordCheck">{this.state.confirmationErrMsg}</p>
                            </div>
                        </Form.Group>
                        <Button className="signupButton mt-3" onClick={() => this.signupSubmit()} variant="primary">
                            登録
                        </Button>
                    </Form>
                </div>
            </>
        )
    }
}

AddUser.propTypes = {
    dispatch: PropTypes.func,
    isSignedIn: PropTypes.bool,
    cookies: PropTypes.any
}

function mapStateToProps(state) {
    return state
}

export default withCookies(connect(mapStateToProps)(AddUser));