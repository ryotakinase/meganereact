import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as actions from '../../action/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { withCookies } from 'react-cookie';
import { Link } from 'react-router-dom';


class UserList extends Component {
  constructor(props) {
    super(props)
    const { dispatch } = props;
    const { cookies } = props;
    this.state = {
      loginUser: cookies.get('user'),
      users: [],
      results: [],
      teams: [],
      sort: ""
    }
    this.action = bindActionCreators(actions, dispatch);
  }

  componentDidMount() {
    fetch("http://localhost:8080/user/get", {
      method: "GET"
    })
      .then((response) => {
        response.json()
          .then(json => {
            this.setState({
              users: json
            });
          });
      })


    fetch("http://localhost:8080/result/get", {
      method: "GET"
    })
      .then((response) => {
        response.json()
          .then(json => {
            this.setState({
              results: json
            });
          });
      })


    fetch("http://localhost:8080/team/get", {
      method: "GET"
    })
      .then((response) => {
        response.json()
          .then(json => {
            this.setState({
              teams: json
            });
          });
      })
  }

  suspendedButtonClicked(user) {
    if (this.state.loginUser.id === user.id) {
      alert("自分の利用権限変更はできません")
    } else {
      var result = window.confirm('対象のユーザーの利用を停止します\nよろしいですか？');
      if (result) {
        const data = { id: user.id, suspended: true };
        fetch("http://localhost:8080/user/suspendedChange", {
          headers: {
            'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify(data)
        })
          .then((response) => {
            if (response.status === 200) {
              response.json()
                .then(json => {
                  this.setState({
                    users: json
                  })

                })
            }
          }).catch(error => console.error(error));
      }
    }
  }


  rebornButtonClicked(user) {
    const data = { id: user.id, suspended: false };
    fetch("http://localhost:8080/user/suspendedChange", {
      headers: {
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify(data)
    })
      .then((response) => {
        if (response.status === 200) {
          response.json()
            .then(json => {
              this.setState({
                users: json
              })
            })
        }
      }).catch(error => console.error(error));
    alert("利用を再開しました。");
  }
  //絞り込み機能
  sortButtonClicked() {
    const str = document.getElementById("sort").value;
    this.setState({
      sortWord: str
    });
  }
  sortResetClicked() {
    this.setState({
      sortWord: ""
    });
    document.getElementById('sort').options[0].selected = true;
  }

  typeList(user) {
    return console.log(user)      
  }



  render() {
    //チーム名表示
    const teamlist = [];
    this.state.teams.map((team) => (
      teamlist[team.id] = team.name
    ));
    //絞り込み機能のためのソート管理
    let userlistElem = []
    //nullの判定は厳密等価演算子を用いることができない
    if (this.state.sortWord === "" || this.state.sortWord == null) {
      userlistElem = this.state.users;
    } else {
      userlistElem = this.state.users.filter(elm => {
        return elm.team === Number(this.state.sortWord)
      });
    }

    //リザルトリストの編纂
    let typeList = [];
    let resultList = this.state.results
    resultList.sort(function(a,b){
      //idは自動振り分けのため、idが大きいほうが新しい結果となる
      /*以下のcurrentTypeList(ユーザーidがキー、タイプが値となる連想配列)の作成において、同一のユーザーidが登場した場合
        先に定義したidとタイプを後に登場したidとタイプで上書きするため、
        降順(最新の結果が後に来るように配列を整理)する(fetch時点でデフォルトで昇順になっているが、一応処理を記述する)*/
      return a.id - b.id;
    });

    for(let i = 0; i < resultList.length; i++){
      let typeDetail = []
      if(resultList[i].driveFlag === true){
        typeDetail.push("ドライブ")
      }
      if(resultList[i].analyzeFlag === true){
        typeDetail.push("アナライズ")
      }
      if(resultList[i].createFlg === true){
        typeDetail.push("クリエイト")
      }
      if(resultList[i].volunteerFlg === true){
        typeDetail.push("ボランティア")
      }
      let str = typeDetail.join('/')
      console.log(str)
      let uId = resultList[i].user.id
      let tip = [uId , str]
      typeList.push(tip)
      /*typeListの中身は[{ユーザーid(重複あり), 結果(文字列、カンマ区切り)}...]の配列であるが
      このままではキーとしてユーザーidを引数にfind関数を用いることができないため、[{ユーザーid(重複なし):結果}...]にしたい*/
    }
    //currentTypeList([{ユーザーid(重複なし):結果}...]のリスト)を作成。結果は最新のもの(typeListの同一ユーザーidに対し最後に現れる結果の値)となる
    const currentTypeList = []
    typeList.map((elem) => (
      currentTypeList[elem[0]] = elem[1]
    ));

    const userlist = userlistElem.map((user) => (
      <tr className="userList-table tr">
        {this.state.loginUser.adminFlg?
          <>
            <td>{user.account}</td>
            <td><Link to={'/users/' + user.id}>{user.name}</Link></td>
            <td>{teamlist[user.team]}</td>
            <td className="currentType">{currentTypeList[user.id] == null ? "未診断" : currentTypeList[user.id]}</td>
              {user.suspended ?
                <td>停止中</td>
              :
                <td>利用可</td>
              }
              {user.suspended ?
                <td><button className="suspendedButton" onClick={() => this.rebornButtonClicked(user)}>復活</button></td> 
              :
                <td><button className="rebornButton" onClick={() => this.suspendedButtonClicked(user)}>停止</button></td> 
              }
              <td><Link to={'/users/edit/' + user.id}><button className="editButton">編集</button></Link></td>
          </>
        :
          <>
            {user.suspended === false &&
              <>
                <td>{user.account}</td>
                <td><Link to={'/users/' + user.id}>{user.name}</Link></td>
                <td>{teamlist[user.team]}</td>
                <td className="currentType">{currentTypeList[user.id] == null ? "未診断" : currentTypeList[user.id]}</td>
              </>
            }
          </>
        }
      </tr>
    ));

    const teamSelect = this.state.teams.map((team) => (
      <option value={team.id}>{team.name}</option>
    ));

    return (
      <div className="top">
        <label>所属絞り込み</label>
        <select id="sort">
          <option value=""></option>
          {teamSelect}
        </select>
        <button className="sortButton" onClick={() => this.sortButtonClicked()}>検索</button>
        <button className="sortButton" onClick={() => this.sortResetClicked()}>リセット</button>
        <table className="table bordered userInfo" >
        <tbody>
          <tr className="userList-table tr">
            <td>社員番号</td>
            <td>名前</td>
            <td>所属</td>
            <td>タイプ</td>
            {this.state.loginUser.adminFlg? <td>状態</td> : <></>}
            {this.state.loginUser.adminFlg? <td></td> : <></>}
            {this.state.loginUser.adminFlg? <td></td> : <></>}
          </tr>
          {userlist}
          </tbody>
        </table>
      </div>
    )
  }S
}

UserList.propTypes = {
  dispatch: PropTypes.func,
  cookies: PropTypes.any,
  users: PropTypes.any,
  teamlist: PropTypes.array,
  sortWord: PropTypes.number,
}

function mapStateToProps(state) {
  return state;
}

export default withCookies(withRouter(connect(mapStateToProps)(UserList)));
