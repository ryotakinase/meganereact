import React, { useEffect, useState, useRef } from 'react'
import { useParams } from 'react-router-dom';
import '../../css/user.css'
import { withRouter } from 'react-router'
import 'whatwg-fetch'
import EditForm from './EditForm';
import { withCookies } from 'react-cookie';


function EditUserAdmin(props) {
  const [editUser, setEditUser] = useState(null)
  // 下記のコードでURLからパラメータを取得できるので、user一覧から遷移する際にuserのIDを渡してください
  const userId = useParams();

  //依存性のある変数をuseEffectで使用する際のレンダリング時に置ける同一性判断
  const prevUserId = useRef(userId);

  // テスト用にidを作成しています
  // const userId = {
  //   id: "1"
  // }

  useEffect(() => {
    fetch("http://localhost:8080/user/get/" + userId.id, {
      method: "GET"
    })
    .then(response => response.json())
    .then(data => setEditUser(data))
    prevUserId.current = userId;
  }, [userId])



  return (
    <>
      {editUser && <EditForm editUser={editUser} />}
    </>
  )
}

export default withCookies(withRouter(EditUserAdmin));