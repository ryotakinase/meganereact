import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/actions'
import { withCookies } from 'react-cookie';
import '../../css/question.css'
import { withRouter } from "react-router";



class QuestionList extends Component {


    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch)
        this.twoPointHandleChange=this.twoPointHandleChange.bind(this)
        this.onePointHandleChange=this.onePointHandleChange.bind(this)
        // userId仮置き
        const { cookies } = this.props;
        this.state = {
            isSignedIn: cookies.get('isSignedIn'),
            user: cookies.get('user'),
            errorMessage:[],
            questionList:[],
            twoPointAnswers:{
                q1_a1:'',
                q2_a1:'',
                q3_a1:'',
                q4_a1:'',
                q5_a1:'',
                q6_a1:'',
                q7_a1:'',
                q8_a1:'',
                q9_a1:'',
                q10_a1:''
            },
            onePointAnswers:{
                q1_a2:'',
                q2_a2:'',
                q3_a2:'',
                q4_a2:'',
                q5_a2:'',
                q6_a2:'',
                q7_a2:'',
                q8_a2:'',
                q9_a2:'',
                q10_a2:''
            },

            totalAnswer:{driveType:0,analyzeType:0,createType:0,volunteerType:0,userId:0}
        
        }
        

    }

    componentDidMount() {
        fetch("http://localhost:8080/question/get", {
            method: "GET"
          })

          .then((response) => {
            response.json()
            .then(json => {
                console.log(json)
                this.setState({questionList:json})
              console.log(this.props);
            })
          })

      }
      
    twoPointHandleChange(e) {
        this.setState({...this.state,twoPointAnswers:{
            ...this.state.twoPointAnswers,[e.target.name]:e.target.value
        }})
    }

    onePointHandleChange(e) {
        this.setState({...this.state,onePointAnswers:{
            ...this.state.onePointAnswers,[e.target.name]:e.target.value
        }})
    }

    render() {

        const twoPointsCheck = Object.values(this.state.twoPointAnswers)
        const onePointsCheck = Object.values(this.state.onePointAnswers)

        const sendButtonClicked = () => {

            var driveAnswer = 0;
            var analyzeAnswer = 0;
            var createAnswer = 0;
            var volunteerAnswer = 0;

            const twoPoints = Object.values(this.state.twoPointAnswers)
            const onePoints = Object.values(this.state.onePointAnswers)

            for(var k = 0; k < 10; k++){
                if(twoPoints[k] !== '' && twoPoints[k]===onePoints[k]){
                    //エラーメッセージを表示して画面を更新しない
                    this.setState({errorMessage:'一番目と二番目が同一の回答があります'}) 
                    return
                }
            }

            for(var i = 0; i < 10; i++){
                if(twoPoints[i]===('drive')){
                    driveAnswer += 2;
                    }
                if(twoPoints[i]===('analyze')){
                    analyzeAnswer += 2;
                    }
                if(twoPoints[i]===('create')){
                    createAnswer += 2;
                    }
                if(twoPoints[i]===('volunteer')){
                    volunteerAnswer += 2;
                    }
                if(twoPoints[i]===('')){
                    //エラーメッセージを表示して画面を更新しない
                    this.setState({errorMessage:'すべての設問に回答してください'})
                    return
                    }
                }

                for(var j = 0; j < 10; j++){
                    if(onePoints[j]===('drive')){
                        driveAnswer += 1;
                        }
                    if(onePoints[j]===('analyze')){
                        analyzeAnswer += 1;
                        }
                    if(onePoints[j]===('create')){
                        createAnswer += 1;
                        }
                     if(onePoints[j]===('volunteer')){
                        volunteerAnswer += 1;
                        }
                    if(onePoints[i]===('')){
                        //エラーメッセージを表示して画面を更新しない
                        this.setState({errorMessage:'すべての設問に回答してください。'})
                        return
                        }
                    }
                    // ここでクッキーからログインユーザ情報を取得、付与
                    const newObject = Object.assign({},this.state.totalAnswer)
                    newObject.driveType=driveAnswer
                    newObject.analyzeType=analyzeAnswer
                    newObject.createType=createAnswer
                    newObject.volunteerType=volunteerAnswer
                    newObject.userId=this.state.user.id
            postSend(newObject)
            }

        
        const postSend = (newObject) => {
            fetch("http://localhost:8080/result/add", {
            method: "POST",
            body: JSON.stringify(newObject)

          })
        // レスポンス返却時の処理
            .then((response) => {
                if(response.status === 200) {
                this.props.history.push("/")
                } else {
                alert("送信に失敗しました。ステータス")
                }
                })
                .catch(error => console.error(error));
            }

        const question = this.state.questionList.map((q,index) =>(
            <form onSubmit={this.handleSubmit} key={q.id}>
            <br/>
            <p className="q-text">Q.{q.text}？</p>
            <table><td className="first">{'一番目'}</td><td className="after">{'に当てはまるものを選んでください'}</td></table>
            {/* <p>{'一番目'}{'に当てはまるものを選んでください'}</p> */}
            <br/>
            <label>
                <input
                disabled={onePointsCheck[index] === 'drive'}
                type="radio"
                name={'q' + q.id + '_a1'}
                value="drive"
                onChange={(e) => this.twoPointHandleChange(e)}
            />
            {q.driveSelect}
            </label>
            <br/>
            <label>
                 <input
                disabled={onePointsCheck[index] === 'analyze'}
                type="radio"
                name={'q' + q.id + '_a1'}
                value="analyze"
                onChange={(e) => this.twoPointHandleChange(e)}
            />
              {q.analyzeSelect}
            </label>
            <br/>
            <label>
                 <input
                disabled={onePointsCheck[index] === 'create'}
                type="radio"
                name={'q' + q.id + '_a1'}
                value="create"
                onChange={(e) => this.twoPointHandleChange(e)}
            />
             {q.createSelect}
            </label>
            <br/>
            <label>
                <input
                disabled={onePointsCheck[index] === 'volunteer'}
                type="radio"
                name={'q' + q.id + '_a1'}
                value="volunteer"
                onChange={(e) => this.twoPointHandleChange(e)}
            />
            {q.volunteerSelect}
            </label>
            <br/>
            <br/>
            <br/>
            <br/>
            <table><td className="second">{'二番目'}</td><td className="after">{'に当てはまるものを選んでください'}</td></table>
            {/* <p>{'二番目に当てはまるものを選んでください'}</p> */}
            <br/>
            <label>
                <input
                disabled={twoPointsCheck[index] === 'drive'}
                type="radio"
                name={'q' + q.id + '_a2'}
                value="drive"
                onChange={(e) => this.onePointHandleChange(e)}
            />
            {q.driveSelect}
            </label>
            <br/>
            <label>
                 <input
                disabled={twoPointsCheck[index] === 'analyze'}
                type="radio"
                name={'q' + q.id + '_a2'}
                value="analyze"
                onChange={(e) => this.onePointHandleChange(e)}
            />
              {q.analyzeSelect}
            </label>
            <br/>
            <label>
                <input
                disabled={twoPointsCheck[index] === 'create'}
                type="radio"
                name={'q' + q.id + '_a2'}
                value="create"
                onChange={(e) => this.onePointHandleChange(e)}
            />
             {q.createSelect}
            </label>
            <br/>
            <label>
                <input
                disabled={twoPointsCheck[index] === 'volunteer'}
                type="radio"
                name={'q' + q.id + '_a2'}
                value="volunteer"
                onChange={(e) => this.onePointHandleChange(e)}
            />
            {q.volunteerSelect}
            </label>
            <br/>
            <br/>
            <br/>
            <br/>
            <p>-----------------------------------------------------------------------------------------</p>
            <br/>
            </form>
        ));
        
          return (
            <div className="table">
            <br/>
            <br/>
            <div className="top">
                 以下の質問に答えて下さい。
            </div>
            <br/>
            <br/>
            <br/>
            
            <div className="question">
                {question}
            </div>
            
            <div className="errorMessage" color="rgb(255, 0, 0)">
                {this.state.errorMessage}
            </div>
            <br/>
            <br/>
            
            <div>
            <button className="button" onClick={sendButtonClicked}>送信</button>
            </div>
           
            <br/>
            <br/>
            
            <div>
            <a className="button" href="./">戻る</a>
            </div>
         
            <br/>
            <br/>
            </div>
        )
    }
}

QuestionList.propTypes = {
    dispatch: PropTypes.func,
    questionList: PropTypes.any,
    cookies: PropTypes.any,
    questionId: PropTypes.number,
    id: PropTypes.number,
    text: PropTypes.string,
    driveSelect: PropTypes.string,
    analyzeSelect: PropTypes.string,
    createSelect: PropTypes.string,
    volunteerSelect: PropTypes.string,
    history: PropTypes.object
}

function mapStateToProps(state) {
    return state
}

export default withCookies(withRouter(connect(mapStateToProps)(QuestionList)));