import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import { bindActionCreators } from 'redux';
import * as actions from '../../action/action';
import { withRouter } from "react-router";

class QuestionElement extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch);
    }

    render() {
        return (
            <table className="inner-list">
                <div className="list-left">
                  <div className="id">【{this.props.id}】</div>
                  <div className="text">【{this.props.text}】</div>
                  <div className="driveSelect">【{this.props.driveSelect}】</div>
                  <div className="analyzeSelect">【{this.props.analyzeSelect}】</div>
                  <div className="createSelect">【{this.props.createSelect}】</div>
                  <div className="volunteerSelect">【{this.props.volunteerSelect}】</div>
                </div>
            </table>
        );
    }
}

QuestionElement.propTypes = {
    dispatch: PropTypes.func,
    cookies: PropTypes.any,
    id: PropTypes.number,
    text: PropTypes.string,
    driveSelect: PropTypes.string,
    analyzeSelect: PropTypes.string,
    createSelect: PropTypes.string,
    volunteerSelect: PropTypes.string
}

function mapStateToProps(state) {
    return state
}
export default withCookies(withRouter(connect(mapStateToProps)(QuestionElement)));
