import React, { useState, useEffect, useRef } from 'react'
import { withRouter } from 'react-router'
import ResultChart from './ResultChart'
import '../../css/result.css'
import { withCookies, useCookies } from 'react-cookie';

function ResultList(props) {
  // ログインユーザーの取得
  const [cookies] = useCookies(['user']);
  const loginUser = cookies.user

  const [resultList, setResultList] = useState(null)

  const prevLoginUser = useRef(loginUser);

  useEffect(() => {
    fetch("http://localhost:8080/result/get/user/" + loginUser.id, {
      method: "GET"
    })
    .then(response => response.json())
    .then(data => setResultList(data))
    prevLoginUser.current = loginUser;
  }, [loginUser]);

  return (
    <div>
      {resultList === null || resultList.length === 0 ?
        <div className="text-center my-5">
          <h2>{loginUser.name}さんは、テスト未実施 です</h2>
          <a href="/question" className="diagnose-button">　診断する　</a>
        </div>
      :
      <>
        <ResultChart resultList={resultList} />
        <p></p>
        <div className="text-center my-5">
          <a href="/question" className="diagnose-button">　診断する　</a>
        </div>
      </>
      }
    </div>
  )
}

export default  withCookies(withRouter(ResultList));