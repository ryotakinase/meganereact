import React from 'react'
import { withRouter } from "react-router";
import { useSelector } from "react-redux";
import { Row, Col } from "react-bootstrap"
import { withCookies } from 'react-cookie';

function PastResult(props) {
  const results = useSelector(state => state.results.list)
  let resultList = results;
    resultList.sort(function(a,b){
      return b.id - a.id;
    })
console.log(results);
  return(
    <Row>
      <Col md="1" />
      <Col md="10">
        {results === null || results.length === 0 ?
          <div className="text-center my-5">
            <h2>過去のテスト結果はございません</h2>
          </div>
        :
          <>
            <h4 className="text-center">過去テスト結果</h4>
            <table className="table table-bordered table-hover table-sm">
              <thead className="text-center thead-light">
                <tr>
                  <th style={{ width: '40%' }}>実施日</th>
                  <th style={{ width: '15%' }}>ドライブ</th>
                  <th style={{ width: '15%' }}>アナライズ</th>
                  <th style={{ width: '15%' }}>クリエイト</th>
                  <th style={{ width: '15%' }}>ボランティア</th>
                </tr>
              </thead>
              <tbody>
              {resultList.map(result => (
                <tr>
                  <td>{result.createdDate.slice(0, 10).replace(/-/g, '/')}</td>
                  <td>{result.driveType}</td>
                  <td>{result.analyzeType}</td>
                  <td>{result.createType}</td>
                  <td>{result.volunteerType}</td>
                </tr>
              ))}
              </tbody>
            </table>
          </>
        }
      </Col>
      <Col md="1" />
    </Row>
  )
}

export default  withCookies(withRouter(PastResult));