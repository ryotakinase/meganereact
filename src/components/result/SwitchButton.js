import React from 'react'
import { withRouter } from "react-router";
import { withCookies } from 'react-cookie';

function SwitchButton({setPrevResult, setNextResult, date}) {

  return(
    <div className="text-center">
      <button onClick={() => setPrevResult()}>◀</button>
      <strong>　{date}実施分　</strong>
      <button onClick={() => setNextResult()}>▶</button>
    </div>
  )
}

export default  withCookies(withRouter(SwitchButton));