import React from 'react'
import { withRouter } from "react-router";
import TypeList from './TypeList';
import ResultList from './resultList';
import PastResult from './PastResult';
import { withCookies } from 'react-cookie';

function Result() {
  return (
    <div>
      <ResultList />
      <TypeList />
      <PastResult />
    </div>
  )
}

export default   withCookies(withRouter(Result));