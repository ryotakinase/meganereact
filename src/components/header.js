import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../action/actions';
import { bindActionCreators } from 'redux';
import { withRouter } from "react-router";
import PropTypes from 'prop-types';
import Logout from './user/logout';
import '../css/header.css';
import { withCookies } from 'react-cookie';

class Header extends Component {
    constructor(props) {
        super(props)
        const { dispatch } = props;
        this.action = bindActionCreators(actions, dispatch);
        const { cookies } = this.props;
        this.state = {
            isSignedIn: cookies.get('isSignedIn'),
            user: cookies.get('user')
        };
    }

    render() {
        return (
            <header className="page-header wrappaer">
                <nav>
                    <ul class="main-nav">
                        {this.state.isSignedIn &&
                            <>
                                <li className="header-button"><a href="/">トップ</a></li>
                                <li className="header-button"><a href="/users">社員一覧</a></li>
                                <li className="header-button"><a href="/users/edit">パスワード変更</a></li>
                                {this.state.user.adminFlg && <li className="header-button"><a href="/signup">利用者登録</a></li>}
                                <li className="logout"><Logout /></li>
                            </>
                        }
                    </ul>
                </nav>
            </header>
        );
    }
}
function mapStateToProps(state) {
    return state
}
Header.propTypes = {
    dispatch: PropTypes.func,
}

export default withCookies(withRouter(connect(mapStateToProps)(Header)));