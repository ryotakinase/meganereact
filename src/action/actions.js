export const UPDATE_RESULT = "UPDATE_RESULT";
export const UPDATE_TYPE = "UPDATE_TYPE";
export const LOGIN = "LOGIN";
export const LOGIN_STATE = "LOGIN_STATE";
export const QUESTION_LIST = "QUESTION_LIST";
export const QUESTION_ELEMENT = "QUESTION_ELEMENT";
export const GET_QUESTIONS = "GET_QUESTIONS";
export function updateResult(result) {
  return {
    type: UPDATE_RESULT,
    payload: result
  }
}

export function updateType(type) {
  return {
    type: UPDATE_TYPE,
    payload: type
  }
}
export function login(loginUser) {
  return {
    type: LOGIN,
    loginUser: loginUser,
  }
}

export function loginState(bool) {
  return {
    type: LOGIN_STATE,
    isLogined: bool,
  }
}

export function questionList(questionList) {
  return {
    type: QUESTION_LIST,
    payload: questionList
  }
}

export function getQuestions(json) {
  return {
      type: 'GET_QUESTIONS',
      questions: json
  }
}