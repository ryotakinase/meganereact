import React from 'react';
import { Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from './components/header'
import{ useCookies } from "react-cookie"

import top from './components/main/top';
import login from './components/user/login';
import edit from './components/user/editUser';
import users from './components/user/userList';
import select from './components/user/selectUser';
import signup from './components/user/signup';
import UserDetail from './components/user/userDetail';
import EditUserAdmin from './components/user/editUserAdmin';
import question from './components/question/questionList';


function App() {
  const [cookies] = useCookies(['user']);
  return (
    <BrowserRouter>
    <Header/>
      <Switch>
      <Route exact path='/login' component={login} />
      {cookies.isSignedIn ?
        <Switch>
        <Route exact path='/' component={top} />
        <Route exact path='/users/edit' component={edit} />
        <Route exact path='/users' component={users} />
        <Route exact path='/users/select' component={select} />
        <Route exact path='/users/edit/:id' component={EditUserAdmin} />
        <Route path='/users/:id' component={UserDetail} />
        <Route path='/signup' component={signup} />
        <Route path='/question' component={question} />
        </Switch>
      :<Redirect to={'/login'} />}      </Switch>
    </BrowserRouter>
  );
};

export default App;
